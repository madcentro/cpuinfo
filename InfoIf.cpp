#include "InfoIf.h"
#include "InfoRow.h"

#include <QProcess>
#include <QQmlContext>

//  This class provides an interface to the cpuinfo
//  When testing, this comes from reading a sample file.
//  On a linux system, I use qprocess to cat /proc/cpuinfo
//  I suspect I could just read /proc/cpuinfo directly, but it seems to be a pseudo magical file,
//  and all the examples I've seen use cat, so to be safe I went with that.
//  Since QProcess provides its output the same as file the code is almost unchanged.

#define testing 1

InfoIf::InfoIf(QQmlContext *ctx, QObject *parent) :
    _viewContext(ctx),
    QObject(parent)
{
    //  Create the process and catch the finished signal.
    _cmdline = new QProcess(this);
    connect(_cmdline, SIGNAL(finished(int)),
            this, SLOT(readInfo()));

    //  run the command to get the cpu info.
    runCommand();
}


void InfoIf::runCommand() {
#ifdef testing
    //  If we're testing, open a sample file and proceed directly to
    //  read the contents.
    _testFile.setFileName("e:/dev/qt/CpuINfo/cpuinfo1.txt");
    if (_testFile.open(QFile::ReadOnly)) {
        readInfo();
    }
#else
    //  On a real linux system, cat the cpuinfo pseudo file.
    QString program("cat");
    QStringList arguments;
    arguments << "/proc/cpuinfo";
    _cmdline->start(program, arguments);
#endif
}

void InfoIf::readInfo() {
    char buf[1024];
    QString line;
    QStringList parts;

    QList<QObject *> infoList;
#ifdef testing
    qint64 lineLength = _testFile.readLine(buf, sizeof(buf));
#else
    qint64 lineLength = _cmdline->readLine(buf, sizeof(buf));
#endif

    while (lineLength != -1) {
        line = QString().fromLatin1(buf);
        parts = line.split(":");

        if (parts.count() == 2) {
            infoList.append(new InfoRow(parts[0].simplified(), parts[1].simplified()));
        }

        //  If there are more than 2 parts, then there was a : in the value.
        //  Reassemble it.
        if (parts.count() > 2) {
            QString value;
            int count = parts.count();
            for (int i=1; i < count; ++i) {
                value.append(parts[i]);
                if (i < count) {
                    value.append(":");
                }
            }
            infoList.append(new InfoRow(parts[0].simplified(), value.simplified()));
        }

#ifdef testing
        lineLength = _testFile.readLine(buf, sizeof(buf));
#else
        lineLength = _cmdline->readLine(buf, sizeof(buf));
#endif
    }

    //  Make the list of info rows visible to qml as a model.
    _viewContext->setContextProperty("infoModel", QVariant::fromValue(infoList));
}
