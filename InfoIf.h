#pragma once

#include <QObject>
#include <QFile>

class QProcess;
class QQmlContext;

class InfoIf : public QObject
{
    Q_OBJECT
public:
    explicit InfoIf(QQmlContext *ctx, QObject *parent = 0);

signals:
    void infoReady();

public slots:
    void readInfo();
    void runCommand();

protected:
    QProcess    *_cmdline;
    QQmlContext *_viewContext;
    QFile       _testFile;

};


