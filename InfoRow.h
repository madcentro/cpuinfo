#pragma once

#include <QObject>

class InfoRow : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString rowName READ rowName WRITE setRowName NOTIFY rowNameChanged)
    Q_PROPERTY(QString rowValue READ rowValue WRITE setRowValue NOTIFY rowValueChanged)

public:
    InfoRow(QObject *parent = 0);
    InfoRow(const QString &name, const QString &value,
            QObject *parent=0);


    QString rowName() const;
    void setRowName(const QString &name);

    QString rowValue() const;
    void setRowValue(const QString &value);

signals:
    void rowNameChanged();
    void rowValueChanged();

private:
    QString _name;
    QString _value;

};

