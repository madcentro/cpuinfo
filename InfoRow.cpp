#include "InfoRow.h"

InfoRow::InfoRow(QObject *parent) :
    QObject(parent)
{
}

InfoRow::InfoRow(const QString &name, const QString &value, QObject *parent) :
    QObject(parent),
    _name(name),
    _value(value)
{
}

QString InfoRow::rowName() const
{
    return _name;
}

void InfoRow::setRowName(const QString &name)
{
    if (name != _name) {
        _name = name;
        emit rowNameChanged();
    }
}

QString InfoRow::rowValue() const
{
    return _value;
}

void InfoRow::setRowValue(const QString &value)
{
    if (value != _value) {
        _value = value;
        emit rowValueChanged();
    }
}
