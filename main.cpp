
#include <QGuiApplication>

#include <qqmlengine.h>
#include <qqmlcontext.h>
#include <qqml.h>
#include <QtQuick/qquickitem.h>
#include <QtQuick/qquickview.h>

#include "InfoIf.h"
#include "InfoRow.h"

int main(int argc, char ** argv)
{
    QGuiApplication app(argc, argv);


    QQuickView view;
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    QQmlContext *ctxt = view.rootContext();

    //  Create the cpuinfo interface object, giving it the qml view context.
    InfoIf info(ctxt);

    view.setSource(QUrl("qrc:main.qml"));
    view.show();

    return app.exec();
}

