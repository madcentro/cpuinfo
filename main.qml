import QtQuick 2.2
import QtQuick.Layouts 1.1

Item {
    visible: true
    width: 400
    height: 800

    property int marginSize: 12
    property int gapSize: 3

    property color headerColor: "#66a"
    property color nameColor: "#44c"
    property color valueColor: "#bbf"
    property color gradTopColor: "#600"
    property color gradBotColor: "#300"

    //  RowLayout couldn't handle size dependencies properly, so for the simple case of two columns, I calcualte column widths by hand.
    function nameWidth(totalWidth) {
        var result = totalWidth / 3;
        if (result < 100) {
            return 100;
        }
        if (result > 200) {
            return 200;
        }
        return result;
    }

    function valueWidth(totalWidth) {
        var leftWidth = nameWidth(totalWidth);
        var result = totalWidth - leftWidth - gapSize;
        if (result < 200) {
            return 200;
        }
        return result;
    }

    Rectangle {
        id: listBG
        gradient: Gradient {
            GradientStop { position: 0.0; color: gradTopColor }
            GradientStop { position: 1.0; color: gradBotColor }
        }

        anchors {
            fill: parent
        }

        Text {
            visible: infoModel.count == 0
            text: "Waiting for Get CPU Info process..."
            anchors.centerIn: parent
            font.pixelSize: 20
            color: "white"
        }

        Item {
            anchors {
                fill: parent
                margins: marginSize
            }

            ListView {
                anchors {
                    fill: parent
                }
                model: infoModel
                spacing: gapSize
                delegate: listDelegate
                clip: true
            }
        }
    }

    Component {
        id: listDelegate

        Item {
            id: rootItem
            //  Rows with the rowName processor are considered headers, and rendered with a separate Rectangle
            property bool isHeader: model.rowName == "processor"

            //  Adjust the height of nonHeader rows to fit contents
            property int valHeight: valueText.height + 10

            width: parent.width
            height: isHeader ? 35 : valHeight

            //  Normal Delegate
            Item {
                visible: !isHeader
                width: parent.width
                height: parent.height

                Rectangle {
                    id: nameRect
                    height: parent.height
                    width: nameWidth(rootItem.width)
                    color: nameColor
                    border {
                        width: 1
                        color: "black"
                    }

                    Text {
                        x: marginSize
                        text: model.rowName
                        color: "white"
                        font.bold: true
                        width: parent.width - (marginSize * 2)
                        anchors {
                            verticalCenter: parent.verticalCenter
                        }
                        elide: Text.ElideRight
                    }
                }

                Rectangle {
                    id: valueRect
                    height: rootItem.height
                    width: valueWidth(rootItem.width)
                    anchors {
                        left: nameRect.right
                        leftMargin: gapSize
                    }

                    color: valueColor
                    border {
                        width: 1
                        color: "black"
                    }

                    Text {
                        id: valueText
                        x: marginSize
                        text: model.rowValue
                        width: parent.width - (marginSize * 2)
                        wrapMode: Text.WordWrap
                        anchors {
                            verticalCenter: parent.verticalCenter
                        }
                    }
                }
            }

            //  Header Delegate
            Rectangle {
                visible: isHeader
                anchors {
                    fill: parent
                }
                color: headerColor
                border {
                    width: 1
                    color: "black"
                }

                Text {
                    text: "Processor: " + model.rowValue
                    color: "white"
                    font.pixelSize: 20
                    anchors {
                        verticalCenter: parent.verticalCenter
                        left: parent.left
                        leftMargin: marginSize
                    }
                }
            }
        }
    }
}
