# README #

This is the CpuInfo exercise.  It was built on Windows in Qt Creator using Qt 5.3.1

It should be entirely self contained.

There is a #define at line 14 of InfoIf.cpp which controls whether it is built for testing or real use on a linux system.  Comment out that line to use on a linux box.  The test files are included in the repository.  The path to the test file being loaded is set in line 34 of the same file.  That path will have to be updated to point to your local install to work.